package com.dane.siena;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SienaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SienaApplication.class, args);
	}

}
