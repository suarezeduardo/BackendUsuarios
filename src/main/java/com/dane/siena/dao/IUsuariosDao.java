package com.dane.siena.dao;
import org.springframework.data.repository.CrudRepository;
import com.dane.siena.entity.Usuarios;

public interface IUsuariosDao extends CrudRepository< Usuarios, Long > {

}
