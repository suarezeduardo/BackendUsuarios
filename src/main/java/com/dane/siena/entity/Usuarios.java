package com.dane.siena.entity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Usuarios")
public class Usuarios implements Serializable {


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	
	private String PrimerNombre;
	private String SegundoNombre;
	private String PrimerApellido;
	private String SegundoApellido;
	private String NumeroIdentificacion;
	private int Edad;
	private String Sexo;
	private String Email;
	private String NumeroCelular;

	
	@Column(name="create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrimerNombre() {
		return PrimerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.PrimerNombre = primerNombre;
	}
	
	public void setSegundoNombre(String segundoNombre) {
		this.SegundoNombre = segundoNombre;
	}

	public String getSegundoNombre() {
		return SegundoNombre;
	}

	

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getPrimerApellido() {
		return PrimerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		PrimerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return SegundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		SegundoApellido = segundoApellido;
	}

	public String getNumeroIdentificacion() {
		return NumeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		NumeroIdentificacion = numeroIdentificacion;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		Edad = edad;
	}

	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String sexo) {
		Sexo = sexo;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getNumeroCelular() {
		return NumeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		NumeroCelular = numeroCelular;
	}

	public static final long serialVersionUID = 1L;
}
