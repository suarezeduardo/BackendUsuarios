package com.dane.siena.controller;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import com.dane.siena.entity.Usuarios;
import com.dane.siena.service.IUsuariosService;

@RestController
@RequestMapping("/api")
public class UsuariosController {
	
	public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
               // .allowedMethods("GET", "POST", "PUT");
    }
	
	@Autowired
	private IUsuariosService usuariosService;

   
	@CrossOrigin(origins = "http://localhost:4200")
	 @GetMapping("/usuarios")
	public List<Usuarios> index() {
		return usuariosService.findAll();
	} 
	
	@CrossOrigin(origins = "http://localhost:4200")
	 @GetMapping("/usuarios/{id}")
		public Usuarios show(@PathVariable Long id) {
			return this.usuariosService.findById(id);
		}

	@CrossOrigin(origins = "http://localhost:4200")
		@PostMapping("/usuarios")
		@ResponseStatus(HttpStatus.CREATED)
		public Usuarios create(@RequestBody Usuarios usuarios) {
			usuarios.setCreateAt(new Date());
			this.usuariosService.save(usuarios);
			return usuarios;
		}

	@CrossOrigin(origins = "http://localhost:4200")
		@PostMapping("/usuariosUpdate")
		@ResponseStatus(HttpStatus.CREATED)
		public Usuarios update(@RequestBody Usuarios usuarios) {
			Usuarios currentUsuarios = this.usuariosService.findById(usuarios.getId());
			currentUsuarios.setPrimerNombre(usuarios.getPrimerNombre());
			currentUsuarios.setSegundoNombre(usuarios.getSegundoNombre());
			currentUsuarios.setPrimerApellido(usuarios.getPrimerApellido());
			currentUsuarios.setSegundoApellido(usuarios.getSegundoApellido());
			currentUsuarios.setNumeroIdentificacion(usuarios.getNumeroIdentificacion());
			currentUsuarios.setEdad(usuarios.getEdad());
			currentUsuarios.setSexo(usuarios.getSexo());
			currentUsuarios.setEmail(usuarios.getEmail());
			currentUsuarios.setNumeroCelular(usuarios.getNumeroCelular());
			this.usuariosService.save(currentUsuarios);
			return currentUsuarios;
		}

		@CrossOrigin(origins = "http://localhost:4200")
		@DeleteMapping("/usuarios/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void delete(@PathVariable Long id) {
			Usuarios currentUsuarios = this.usuariosService.findById(id);
			this.usuariosService.delete(currentUsuarios);
		}

}
