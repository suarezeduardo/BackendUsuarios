package com.dane.siena.service;
import java.util.List;
import com.dane.siena.entity.Usuarios;


public interface IUsuariosService {

	public void save(Usuarios usuarios);
	
	public List<Usuarios> findAll();
	
	public Usuarios findById(Long id);
	
	public void delete(Usuarios usuarios);
}

