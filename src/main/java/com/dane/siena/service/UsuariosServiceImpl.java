
package com.dane.siena.service;
import java.util.List;
import javax.persistence.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.dane.siena.dao.IUsuariosDao;
import com.dane.siena.entity.Usuarios;

@Service
public class UsuariosServiceImpl implements IUsuariosService {
	
	@Autowired
	private IUsuariosDao usuariosDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Usuarios> findAll() {
		return (List<Usuarios>) usuariosDao.findAll();
	}
	
	@Override
	@Transactional
	public void save(Usuarios usuarios) {
		usuariosDao.save(usuarios);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuarios findById(Long id) {
		return usuariosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(Usuarios usuarios) {
		usuariosDao.delete(usuarios);
		
	}

}
